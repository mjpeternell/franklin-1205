<?php
/**
* The main template file
*
* This is the most generic template file in a WordPress theme
* and one of the two required files for a theme (the other being style.css).
* It is used to display a page when nothing more specific matches a query.
* e.g., it puts together the home page when no home.php file exists.
*
* Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
*
* @package FoundationPress
* @since FoundationPress 1.0.0
*/

get_header(); ?>
<?php //get_template_part( 'template-parts/featured-image' ); ?>

<!-- Reveal Modals end -->
<main class="main-parallax">
    <?php if( have_rows('home_page_repeater','options') ): ?>
        <div class="vs-slider">
            <?php $count = 0; ?>
            <?php while ( have_rows('home_page_repeater', 'options') ) : the_row(); ?>
                <?php
                $count++;
                $post_object = get_sub_field('po', 'options');
                $image = get_sub_field('po_background_image', 'options');
                $slug = get_post_field( 'post_name', $post_object->ID );
                if( !empty($image) ):
                    // vars
                    $url = $image['url'];
                endif;

                if($count == 1) {
                    $my_slug = "home";
                } else {
                    $my_slug = $slug;
                }
                ?>
                <?php if( $post_object): ?>
                    <?php $post = $post_object; setup_postdata( $post ); ?>
                    <section class="vs-section-a parallax-window" data-parallax="scroll" data-image-src="<?php echo $url; ?>" id="<?php echo $my_slug; ?>"  data-slug="<?php echo $slug; ?>" rel="<?php echo $count; ?>">
                            <div class="mobile-screen"></div>
                            <div class="content-wrapper">
                                <?php
                                if($my_slug == "home") {
                                    get_template_part( 'template-parts/post-object-home' );
                                } elseif($my_slug == "what-i-am-about") {
                                    get_template_part( 'template-parts/post-object-what-about' );
                                } elseif($my_slug == "wear-me") {
                                    get_template_part( 'template-parts/post-object-wear-me' );
                                } elseif($my_slug == "who-i-am") {
                                    get_template_part( 'template-parts/post-object-who-i-am' );
                                } elseif($my_slug == "what-i-do") {
                                    get_template_part( 'template-parts/post-object-wid' );
                                } elseif($my_slug == "contact-me") {
                                    get_template_part( 'template-parts/post-object-contact-me' );
                                } elseif($my_slug == "my-experiences") {
                                    get_template_part( 'template-parts/post-object-my-experiences' );
                                } elseif($my_slug == "follow-me") {
                                    get_template_part( 'template-parts/post-object-follow-me' );
                                } elseif($my_slug == "book-me") {
                                    get_template_part( 'template-parts/post-object-book-me' );
                                } else {
                                    return false;
                                }
                                ?>

                            </div>

                    </section>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>
            <?php endwhile; ?>
        </div>
        <?php get_template_part('template-parts/reveal','shows'); ?>
        <?php get_template_part('template-parts/reveal','vlog'); ?>
        <?php get_template_part('template-parts/reveal','bookme'); ?>
        <?php get_template_part('template-parts/reveal','gallery'); ?>
        <?php //get_template_part('template-parts/reveal','store'); ?>
    <?php endif; ?>
</main>
<?php get_footer();
