<?php
/**
 * The sidebar containing the main widget area
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<aside class="sidebar">
	<?php do_action('my_xcart'); ?>
	
	<?php dynamic_sidebar( 'sidebar-widgets' ); ?>
</aside>
