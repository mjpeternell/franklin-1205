<?php
/**
* The template for displaying the header
*
* Displays all of the head element and everything up until the "container" div.
*
* @package FoundationPress
* @since FoundationPress 1.0.0
*/

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<?php wp_head(); ?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/scrollify/1.0.19/jquery.scrollify.min.js"></script>
</head>
<?php
if(is_shop()) {
	$is_shop = "is-shop";
} else {
	$is_shop = "";
}
?>
<body <?php body_class($is_shop); ?>>
	<button aria-label="<?php _e( 'Main Menu', 'foundationpress' ); ?>" class="menu-icon" type="button" data-toggle="<?php foundationpress_mobile_menu_id(); ?>"></button>
	<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
		<?php get_template_part( 'template-parts/mobile-off-canvas' ); ?>
	<?php endif; ?>

	<header class="site-header" role="banner">
		<div class="site-title-bar title-bar" <?php foundationpress_title_bar_responsive_toggle(); ?>>
			<?php get_template_part('template-parts/acf', 'social'); ?>
			<div class="site-title">
				<a href="/">FRANKLIN MARSHALL III</a>
			</div>
		</div>
	</header>
