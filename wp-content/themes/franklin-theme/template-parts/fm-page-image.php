<?php
$franklin_page_image = get_field('franklin_page_image');

if( !empty($franklin_page_image) ):
    // vars
    $fm_url = $franklin_page_image['url'];
    $fm_title = $franklin_page_image['title'];
    $fm_alt = $franklin_page_image['alt'];
    $fm_caption = $franklin_page_image['caption'];
    ?>
    <div class="fm-featured-img">
        <img src="<?php echo $fm_url; ?>" alt="<?php echo $fm_alt; ?>" class="img-responsive series-thing-1"/>
    </div>
<?php endif; ?>
