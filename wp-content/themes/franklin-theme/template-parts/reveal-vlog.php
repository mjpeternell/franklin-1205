<!-- Full screen modal  data-animation-in="scale-in-up" data-animation-out="scale-out-down" -->
<div class="full reveal vlog-reveal" id="vlogModal" data-reveal data-close-on-click="true"  data-animation-in="fm--hinge-in" data-animation-out="fm--hinge-out">
    <div class="wrapper">
        <div class="anime-bar hidden"></div>
    </div>
    <div class="content">
        <a class="back-link hidden" href="/">FRANKLIN MARSHALL III</a>
        <button class="close-button hidden" data-close aria-label="Close reveal" type="button">
            <span class="hide-for-small-only">Back to the main site</span><span class="close-icon" aria-hidden="true">&times;</span>
        </button>
        <h2 class="page-title text-left hidden">I started a vlog series...kay</h2>
        <div class="grid-container fluid vlog-content">
        <div class="grid-x grid-margin-x">
                <?php
                // WP_Query arguments
                $args = array(
                    'post_type'              => array( 'videos' ),
                    'post_status'            => array( 'publish' ),
                    'order'                  => 'DESC',
                    'orderby'                => 'date',
                );

                // The Query
                $v_query = new WP_Query( $args );

                // The Loop
                if ( $v_query->have_posts() ) {
                    while ( $v_query->have_posts() ) {
                        $v_query->the_post();
                        // do something
                        ?>
                        <div class="cell small-12 medium-6 large-4 vpost hidden">
                            <div class="grid-x grid-margin-x inner">
                                <div class="cell large-6 featured-img">
                                    <?php //get_template_part('template-parts/acf-oembed-vlog'); ?>
                                    <?php $video_embed = get_field('video_embed'); ?>
                                    <?php
                                    if ($video_embed != ""){ ?>
                                        <a href="<?php echo $video_embed; ?>" target="_blank" title="<?php the_title(); ?>">
                                    <?php } ?>
                                    <?php
                                    if ( has_post_thumbnail() ) {
                                        the_post_thumbnail('medium');
                                    }
                                    ?>
                                    <?php if ($video_embed != ""){ ?>
                                    </a>
                                    <?php } ?>
                                </div>
                                <div class="cell large-6">
                                    <h2>
                                        <?php
                                        if ($video_embed != ""){ ?>
                                            <a href="<?php echo $video_embed; ?>" target="_blank" title="<?php the_title(); ?>">
                                        <?php } ?>
                                        <?php the_title(); ?>
                                        <?php if ($video_embed != ""){ ?>
                                        </a>
                                        <?php } ?>
                                    </h2>
                                    <?php
                                    $video_excerpt = get_field('video_excerpt');
                                    if($video_excerpt != '') {
                                        echo '<p>'.$video_excerpt.'</p>';
                                    }
                                    ?>
                                    <div class="vmeta">
                                        <div class="views">2,000 views</div>
                                        <div class="pub-date"><?php the_time('M j, Y') ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                } else {
                    // no posts found
                }

                // Restore original Post Data
                wp_reset_postdata();
                ?>

        </div>
        </div>
    </div> 
</div>
