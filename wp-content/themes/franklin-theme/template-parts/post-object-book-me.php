<div class="content-inner">
    <?php
    $fm_page_title = get_field("fm_page_title");
    if( $fm_page_title ) { ?>
        <h1 class="hidden"><?php echo $fm_page_title; ?></h1>
    <?php } else { ?>
        <h1 class="hidden"><?php the_title(); ?></h1>
    <?php } ?>
    <div  >
        <?php
        $fm_page_content = get_field("fm_page_content");
        if( $fm_page_content ) { ?>
            <p class="description hidden"><?php echo $fm_page_content; ?></p>
        <?php } ?>
        <div class="link-section dashes short">
            <div class="dividers hidden">>> ---------------------------------------------------------</div>
            <p><span class="dash-btn hidden">BOOK ME</span></p>
            <!-- <a class="button book-me-btn my-show-btn hidden" id="bookMeBtn" data-toggle="exampleModal8" data-rjs="2">Book Me</a> -->
            <div class="circular-modalbtn-wrapper showsBtn">
                    <div class="outer-circle hidden"></div>
                    <div class="inner-circle hidden"></div>
                    <a class="button book-me-btn my-show-btn hidden" id="bookMeBtn">Shows</a>
                </div>
        </div>
    </div>
</div>
<?php get_template_part( 'template-parts/fm-page-image' ); ?>
