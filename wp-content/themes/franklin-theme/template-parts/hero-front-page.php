<?php

/**
* Home page Hero Template
*
* Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
*
* @package FoundationPress
* @since FoundationPress 1.0.0
*/

?>
<?php
if( get_field('caption_position') == 'left' ):
	$capt_pos = "left-cpt";
elseif (get_field('caption_position') == 'center' ):
    $capt_pos = "center-cpt";
endif;
?>

<?php if( have_rows('hero_slides') ): ?>
<div class="grid-container full">
    <div class="grid-x">
        <div class="cell large-12">
            <div class="slick-slider">
                <?php while( have_rows('hero_slides') ): the_row();

                // vars
                $slide_title = get_sub_field('slide_title');
                $slide_sub_title = get_sub_field('slide_sub_title');
                $slide_lead_copy = get_sub_field('slide_lead_copy');
                $slide_image = get_sub_field('slide_image');
                $slide_button_label = get_sub_field('slide_button_label');
                $link = get_sub_field('slide_button_url');
                $featured_small = $slide_image ['sizes']['featured-small'];
                $featured_medium = $slide_image ['sizes']['featured-medium'];
                $featured_large = $slide_image ['sizes']['featured-large'];
                $featured_xlarge = $slide_image ['sizes']['featured-xlarge'];

                $link_url = "";
                $link_target = "";

                if ($link ) {
                    $link_title = $link['title'];
                    $link_url = $link['url'];
                    $link_target = $link['target'];
                }

                if ($link_target) {
                    $my_target = 'target="'.$link_target.'"';
                } else {
                    $my_target = "";
                }
                ?>
                <div>
                    <div class="featured-hero" role="banner" data-interchange="[<?php echo $featured_small; ?>, small], [<?php echo $featured_medium; ?>, medium], [<?php echo $featured_large; ?>, large], [<?php echo $featured_xlarge; ?>, xlarge]">
                        <div class="caption-container">
                            <div class="caption <?php echo $capt_pos; ?>">
                                <?php if( $slide_sub_title !='') { ?>
                                    <h5><?php echo $slide_sub_title; ?></h5>
                                <?php } ?>
                                <?php if( $slide_title !='') { ?>
                                    <h1 class="entry-title"><?php echo $slide_title; ?></h1>
                                <?php } ?>
                                <?php if( $slide_lead_copy !='') { ?>
                                    <p class="lead"><?php echo $slide_lead_copy; ?>
                                <?php } ?>
                                <?php if( $link_url !='') { ?>
                                    <p><a class="button" href="<?php echo $link_url; ?>" title="<?php echo $link_title; ?>" <?php echo $my_target; ?>><?php echo $slide_button_label; ?></a>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="white-curve"></div>
                		<div class="dark-filter"></div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
</div>

<?php endif; ?>
