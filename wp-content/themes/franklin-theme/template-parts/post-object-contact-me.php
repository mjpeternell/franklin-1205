<div class="content-inner" >
    <?php
    $contact_me_title = get_field("contact_me_title");
    if( $contact_me_title ) { ?>
        <h1><?php echo $contact_me_title; ?></h1>
    <?php } else { ?>
        <h1><?php the_title(); ?></h1>
    <?php } ?>
    <?php
    $contact_me_content = get_field("contact_me_content");
    if( $contact_me_content ) { ?>
        <p><?php echo $contact_me_content; ?></p>
    <?php } ?>

    <?php
    $contact_form = get_field("contact_form");
    if( $contact_form ) { ?>
        <p><?php echo do_shortcode($contact_form); ?></p>
    <?php } ?>
</div>
<?php get_template_part( 'template-parts/featured-image-contact-me' ); ?>
