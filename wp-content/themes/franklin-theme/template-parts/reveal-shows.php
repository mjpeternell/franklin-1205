<!-- Full screen modal data-animation-in="scale-in-up" data-animation-out="scale-out-down" -->
<div class="full reveal shows-reveal" id="showsModal" data-reveal data-animation-in="fm--hinge-in" data-animation-out="fm--hinge-out" data-close-on-click="true" >
    <a class="back-link" href="/">FRANKLIN MARSHALL III</a>
    <button class="close-button" data-close aria-label="Close reveal" type="button">
        <span class="hide-for-small-only">Back to the main site</span><span class="close-icon" aria-hidden="true">&times;</span>
    </button>
    <div class="grid-container fluid">
        <div class="grid-x grid-margin-x">
                <div class="cell small-offset-0 small-12 medium-offset-0 medium-12 large-offset-3 large-9">
                    <h2 class="reveal-title">Upcoming shows</h2>
                </div>
            <div class="cell small-offset-0 small-12 medium-offset-0 medium-12 large-offset-3 large-9">
            <div class="grid-x grid-margin-x event-calendar">
                <?php
                $args = array(
                    'post_status'=>'publish',
                    'post_type'=>array(Tribe__Events__Main::POSTTYPE),
                    'posts_per_page'=>10,
                    //order by startdate from newest to oldest
                    'meta_key'=>'_EventStartDate',
                    'orderby'=>'_EventStartDate',
                    'order'=>'ASC',
                    //required in 3.x
                    'eventDisplay'=>'custom',
                );
                $get_posts = null;
                $get_posts = new WP_Query();
                $get_posts->query($args);
                if($get_posts->have_posts()) : while($get_posts->have_posts()) : $get_posts->the_post(); ?>
                <?php

                ?>
                <div class="cell small-12 medium-2 text-center">
                    <div class="cell-inner">
                        <span class="month"><?php echo tribe_get_start_date($post->ID, false,'M ') ?> </span>
                        <span class="day"><?php echo tribe_get_start_date($post->ID, false,'j') ?></span>
                    </div>
                </div>
                <div class="cell small-12 medium-5 show-title">
                    <div class="cell-inner">
                        <h4 class="title"><?php the_title(); ?></h4>
                        <?php
                        // if (tribe_get_start_date() !== tribe_get_end_date() ) {
                        //     echo tribe_get_start_date($post->ID, false,'D g:ia') .  " - " . tribe_get_end_date($post->ID, false,'D g:ia');
                        // } else {
                        //     echo tribe_get_start_date($post->ID, false,'D g:ia');
                        // }
                        ?>
                        <span class="event-time"><?php echo tribe_get_start_date($post->ID, false,'D g:ia') ?></span>
                    </div>
                </div>
                <div class="cell small-12 medium-3 venue">
                    <div class="cell-inner">
                        <h4><?php echo tribe_get_venue(); ?></h4>
                        <span class="city-state"><?php echo tribe_get_city(); ?>, <?php echo tribe_get_state(); ?></span>
                    </div>
                </div>
                <div class="cell small-12 medium-2 tickets">
                    <div class="cell-inner">
                        <a href="<?php echo tribe_get_event_website_url(); ?>" target="_blank" class="button">Tickets</a>
                    </div>
                </div>
            <?php  endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
            </div>
            </div>
        </div>

    </div>
</div>
