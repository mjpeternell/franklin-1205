<?php
/*
 * ACF Theme Option Social Links - Header or Footer.
 *
 */
?>
<?php if (have_rows('social_links', 'option')): ?>
    <ul class="social-links-list ">
        <?php
        $contact_page_link = get_field('contact_page_link','option');
        if ($contact_page_link) : ?>
        <li class="contact-link"><a href="<?php echo $contact_page_link; ?>"><span class="hide-for-small-only">Contact Us</span><i class="fa fa-phone show-for-small-only" aria-hidden="true"></i></a></li>
        <?php endif; ?>
        <?php
        while (have_rows('social_links', 'option')): the_row();
            // vars
            $social_icon = get_sub_field('social_icon', 'option');
            $social_title = get_sub_field('social_title', 'option');
            $social_icon_url = get_sub_field('social_icon_url', 'option');
            ?>
            <li class="social-link">  <a href="<?php echo $social_icon_url; ?>" title="<?php echo $social_title; ?>" target="_blank"> <?php echo $social_icon; ?></a></li>
            <?php endwhile; ?>

    </ul>
<?php endif; ?>
