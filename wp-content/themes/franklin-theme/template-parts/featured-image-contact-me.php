<?php
$contact_me_pg_left_image = get_field('contact_me_pg_left_image');

if( !empty($contact_me_pg_left_image) ):
    // vars
    $contact_left_url = $contact_me_pg_left_image['url'];
    $contact_left_title = $contact_me_pg_left_image['title'];
    $contact_left_alt = $contact_me_pg_left_image['alt'];
    $contact_left_caption = $contact_me_pg_left_image['caption'];
    ?>
    <div class="fm-featured-img left">
        <img src="<?php echo $contact_left_url; ?>" alt="<?php echo $contact_left_alt; ?>" class="img-responsive" />
    </div>
<?php endif; ?>

<?php
$contact_me_pg_right_image = get_field('contact_me_pg_right_image');

if( !empty($contact_me_pg_right_image) ):
    // vars
    $contact_right_url = $contact_me_pg_right_image['url'];
    $contact_right_title = $contact_me_pg_right_image['title'];
    $contact_right_alt = $contact_me_pg_right_image['alt'];
    $contact_right_caption = $contact_me_pg_right_image['caption'];
    ?>
    <div class="fm-featured-img right">
        <img src="<?php echo $contact_right_url; ?>" alt="<?php echo $contact_right_alt; ?>" class="img-responsive" />
    </div>
<?php endif; ?>
