<?php
// If a featured image is set, insert into layout and use Interchange
// to select the optimal image size per named media query.
if ( !is_home() && !is_archive() && has_post_thumbnail( $post->ID ) ) : ?>
	<header class="featured-hero" role="banner" data-interchange="[<?php the_post_thumbnail_url( 'featured-small' ); ?>, small], [<?php the_post_thumbnail_url( 'featured-medium' ); ?>, medium], [<?php the_post_thumbnail_url( 'featured-large' ); ?>, large], [<?php the_post_thumbnail_url( 'featured-xlarge' ); ?>, xlarge]">
		<div class="caption center-cpt">
			<h1 class="entry-title"><?php the_title(); ?></h1>
			<ul class="post-cats">
				<?php
					// get post categories
					$cats = wp_get_post_categories($post->ID);
					foreach ( $cats as $category ) {
						echo "<li>";
						$current_cat = get_cat_name($category);
						$cat_link = get_category_link($category);
						echo "<a href='$cat_link'>";
						echo $current_cat;
						echo "</a>";
						echo "</li>";
					}
				?>
			</ul>
		</div>
		<div class="white-curve"></div>
		<div class="dark-filter"></div>
	</header>
<?php endif;?>

<?php if ( is_archive()) : ?>
	<?php

	$archive_hero_image = get_field('archive_hero_image', 'options');
	$archive_hero_image['sizes'][''];
	?>
	<header class="featured-hero" role="banner" data-interchange="[<?php echo $archive_hero_image['sizes']['featured-small']; ?>, small], [<?php echo $archive_hero_image['sizes']['featured-medium']; ?>, medium], [<?php echo $archive_hero_image['sizes']['featured-large']; ?>, large], [<?php echo $archive_hero_image['sizes']['featured-xlarge']; ?>, xlarge]">
		<div class="caption center-cpt">
			<h1 class="entry-title"><?php the_archive_title();?></h1>
		</div>
		<div class="white-curve"></div>
		<div class="dark-filter"></div>
	</header>
<?php endif;?>



<?php if ( is_home() ) : ?>
<?php
$xlarge = wp_get_attachment_image_src(get_post_thumbnail_id(get_option('page_for_posts')),'featured-xlarge');
$xlarge_image = $xlarge[0];

$large = wp_get_attachment_image_src(get_post_thumbnail_id(get_option('page_for_posts')),'featured-large');
$large_image = $large[0];

$features_medium = wp_get_attachment_image_src(get_post_thumbnail_id(get_option('page_for_posts')),'featured-medium');
$features_medium = $features_medium[0];

$featured_small = wp_get_attachment_image_src(get_post_thumbnail_id(get_option('page_for_posts')),'featured-small');
$featured_small = $featured_small[0];

$post_page_id = get_option('page_for_posts');
$parent_title = get_the_title( $post_page_id );
?>
	<header class="featured-hero" role="banner" data-interchange="[<?php echo $featured_small; ?>, small], [<?php echo $features_medium; ?>, medium], [<?php echo $large_image; ?>, large], [<?php echo $xlarge_image; ?>, xlarge]">
		<div class="caption center-cpt">
			<h1 class="entry-title"><?php echo $parent_title; ?></h1>
		</div>
		<div class="white-curve"></div>
		<div class="dark-filter"></div>
	</header>
<?php endif;?>
