<div class="content-inner" >
    <div>
    <?php
    $fm_page_title = get_field("fm_page_title");
    if( $fm_page_title ) { ?>
        <h1 class="hidden"><?php echo $fm_page_title; ?></h1>
    <?php } else { ?>
        <h1 class="hidden"><?php the_title(); ?></h1>
    <?php } ?>
        <?php
        $fm_page_content = get_field("fm_page_content");
        if( $fm_page_content ) { ?>
            <p class="description hidden"><?php echo $fm_page_content; ?></p>
            <div class="link-section dashes blk">
                <div class="dividers hidden">>> ---------------------------------------------------------</div>
                <p><span class="dash-btn hidden">CHECK OUT MY MANY VIDEOS</span></p>
                <a class="button my-show-btn hidden" id="vlogModalBtn" >vlog</a>
            </div>
        <?php } ?>
    </div>
</div>
<?php get_template_part( 'template-parts/fm-page-image' ); ?>
