<!-- Full screen modal -->
<div class="full reveal gallery-reveal" id="galleryModal" data-reveal data-close-on-click="true"  data-animation-in="fm--hinge-in" data-animation-out="fm--hinge-out">
    <a class="back-link" href="/">FRANKLIN MARSHALL III</a>
    <button class="close-button" data-close aria-label="Close reveal" type="button">
        <span class="hide-for-small-only">Back to the main site</span><span class="close-icon" aria-hidden="true">&times;</span>
    </button>
    <div class="grid-container fluid">
        <div class="grid-container">
            <div class="grid-x grid-margin-x">
                <!-- <div class="cell small-12 large-12">
                    <h2 class="text-center">Gallery</h2>
                </div> -->
                <div class="cell small-offset-1 small-10 medium-offset-1 medium-10 large-offset-0 large-12">
                    <div class="slick-area">
                    <?php
                    $fm_photo_gallery = get_field('fm_photo_gallery', 'options');
                    $size_med = 'featured-gallery'; // (thumbnail, medium, large, full or custom size)
                    $fp_xsmall = 'fp-xsmall'; // (thumbnail, medium, large, full or custom size)

                    if( $fm_photo_gallery ): ?>
                    <div class="slick-gallery slick-slider">
                        <?php foreach( $fm_photo_gallery as $img ): ?>
                            <div>
                                <?php echo wp_get_attachment_image( $img['ID'], $size_med ); ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
                <?php if( $fm_photo_gallery ): ?>
                    <div class="slider-thumb slick-slider">
                        <?php foreach( $fm_photo_gallery as $img ): ?>
                            <div>
                                <?php echo wp_get_attachment_image( $img['ID'], $fp_xsmall ); ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
