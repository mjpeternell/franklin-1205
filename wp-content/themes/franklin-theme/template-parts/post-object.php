<div class="content-inner">
    <?php
    $fm_page_title = get_field("fm_page_title");
    if( $fm_page_title ) { ?>
        <h1><?php echo $fm_page_title; ?></h1>
    <?php } else { ?>
        <h1><?php the_title(); ?></h1>
    <?php } ?>
    <?php
    $fm_page_content = get_field("fm_page_content");
    if( $fm_page_content ) { ?>
        <p><?php echo $fm_page_content; ?></p>
    <?php } ?>
</div>
<?php get_template_part( 'template-parts/fm-page-image' ); ?>
