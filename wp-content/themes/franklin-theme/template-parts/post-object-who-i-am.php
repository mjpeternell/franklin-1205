<div class="content-inner"  >
    <?php
    $fm_page_title = get_field("fm_page_title");
    if( $fm_page_title ) { ?>
        <h1 class="hidden"><?php echo $fm_page_title; ?></h1>
    <?php } else { ?>
        <h1 class="hidden"><?php the_title(); ?></h1>
    <?php } ?>
    <div>
        <?php
        $fm_page_content = get_field("fm_page_content");
        if( $fm_page_content ) { ?>
            <p class="description hidden"><?php echo $fm_page_content; ?></p>
            <div class="link-section dashes">
                <div class="dividers hidden">>> ---------------------------------------------------------</div>
                <p><span class="dash-btn hidden">SEE WHERE I'M PERFORMING NEXT</span></p>
                <div class="circular-modalbtn-wrapper showsBtn">
                    <div class="outer-circle hidden"></div>
                    <div class="inner-circle hidden"></div>
                    <img src="/wp-content/themes/franklin-theme/dist/assets/images/microphone.png" class="image mic hidden" />
                    <a class="button my-show-btn hidden" id="showsBtn">Shows</a>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<?php get_template_part( 'template-parts/fm-page-image' ); ?>
