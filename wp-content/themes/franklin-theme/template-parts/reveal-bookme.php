<!-- Full screen modal -->
<div class="full reveal book-me-reveal" id="exampleModal8" data-reveal data-close-on-click="true"  data-animation-in="scale-in-up" data-animation-out="scale-out-down">
    <a class="back-link" href="/">FRANKLIN MARSHALL III</a>
    <button class="close-button" data-close aria-label="Close reveal" type="button">
        <span class="hide-for-small-only">Back to the main site</span><span class="close-icon" aria-hidden="true">&times;</span>
    </button>
    <div class="grid-container fluid">
      <div class="grid-x grid-margin-x">
          <div class="cell small-12 large-12">
              <h2 class="text-center">Book Me</h2>
          </div>
        <div class="cell small-offset-1 small-10 medium-offset-1 medium-10 large-offset-4 large-4"><?php echo do_shortcode( '[contact-form-7 id="65" title="Contact form 1"]' ); ?></div>
      </div>
    </div>
</div>
