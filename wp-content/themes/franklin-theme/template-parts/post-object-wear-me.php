
<?php
$wm_image_left = get_field('wm_image_left');
if( !empty($wm_image_left) ):
    // vars
    $wm_left_url = $wm_image_left ['url'];
    $wm_left_title = $wm_image_left ['title'];
    $wm_left_alt = $wm_image_left ['alt'];
    $wm_left_caption = $wm_image_left['caption'];
    ?>
    <div class="wid-img left fm-featured-img">
        <img src="<?php echo $wm_left_url; ?>" alt="<?php echo $wm_left_alt; ?>" class="img-responsive" />
    </div>
<?php endif; ?>

<?php
$wm_right_image = get_field('wm_image_right');

if( !empty($wm_right_image) ):
    // vars
    $wm_right_url = $wm_right_image['url'];
    $wm_right_title = $wm_right_image['title'];
    $wm_right_alt = $wm_right_image['alt'];
    $wm_right_caption = $wm_right_image['caption'];
    ?>
    <div class="wid-img right fm-featured-img">
        <img src="<?php echo $wm_right_url; ?>" alt="<?php echo $wm_right_alt; ?>" class="img-responsive" />
    </div>
<?php endif; ?>

<div class="content-inner">
    <?php
    $fm_page_title = get_field("fm_page_title");
    if( $fm_page_title ) { ?>
        <h1 class="hidden"><?php echo $fm_page_title; ?></h1>
    <?php } else { ?>
        <h1 class="hidden"><?php the_title(); ?></h1>
    <?php } ?>
    <div>
    <?php
    $fm_page_content = get_field("fm_page_content");
    if( $fm_page_content ) { ?>
        <p class="description hidden"><?php echo $fm_page_content; ?></p>
    <?php } ?>
        <p></p>
        <div class="link-section dashes short">
            <div class="dividers hidden">>> ---------------------------------------------------------</div>
            <p><span class="dash-btn hidden">WEAR ME</span></p>
            <a href="/shop" class="button wear-me-btn my-show-btn hidden" id="storeBtn" data-rjs="2">Store</a>
        </div>
    </div>
</div>
<?php get_template_part( 'template-parts/fm-page-image' ); ?>
