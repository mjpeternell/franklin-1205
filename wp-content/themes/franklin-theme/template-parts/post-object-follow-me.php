<div class="content-inner">
    <?php
    $fm_page_title = get_field("fm_page_title");
    if( $fm_page_title ) { ?>
        <h1 class="hidden"><?php echo $fm_page_title; ?></h1>
    <?php } else { ?>
        <h1 class="hidden"><?php the_title(); ?></h1>
    <?php } ?>
    <div  >
        <?php
        $fm_page_content = get_field("fm_page_content");
        if( $fm_page_content ) { ?>
            <p class="description hidden"><?php echo $fm_page_content; ?></p>
        <?php } ?>

        <div class="social-links icons hidden">
            <?php get_template_part('template-parts/acf-social'); ?>
        </div>
    </div>
</div>
<?php get_template_part( 'template-parts/fm-page-image' ); ?>
