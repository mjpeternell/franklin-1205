<div class="content-inner">
    <?php
    $wid_title = get_field("wid_title");
    if( $wid_title ) { ?>
        <h1 class="hidden" ><?php echo $wid_title; ?></h1>
    <?php } else { ?>
        <h1 class="hidden" ><?php the_title(); ?></h1>
    <?php } ?>
    <?php
    $wid_content = get_field("wid_content");
    if( $wid_content ) { ?>
        <p class="description hidden"><?php echo $wid_content; ?></p>
    <?php } ?>
</div>
<?php
$wid_left_image = get_field('wid_left_image');
// /wp-content/themes/franklin-theme/dist/assets/images/what_i_do_beach.png

if( !empty($wid_left_image) ):
    // vars
    $wid_left_url = $wid_left_image['url'];
    $wid_left_title = $wid_left_image['title'];
    $wid_left_alt = $wid_left_image['alt'];
    $wid_left_caption = $wid_left_image['caption'];
    ?>
    <div class="wid-img left fm-featured-img">
        <img src="<?php echo $wid_left_url; ?>" alt="<?php echo $wid_left_alt; ?>" class="img-responsive" />
    </div>
<?php endif; ?>

<?php
$wid_right_image = get_field('wid_right_image');

if( !empty($wid_right_image) ):
    // vars
    $wid_right_url = $wid_right_image['url'];
    $wid_right_title = $wid_right_image['title'];
    $wid_right_alt = $wid_right_image['alt'];
    $wid_right_caption = $wid_right_image['caption'];
    ?>
    <div class="wid-img right fm-featured-img">
        <img src="<?php echo $wid_right_url; ?>" alt="<?php echo $wid_right_alt; ?>" class="img-responsive" />
    </div>
<?php endif; ?>

<?php
$wid_basketball = get_field('basketball_image');

if( !empty($wid_basketball) ):
    // vars
    $wid_basketball_url = $wid_basketball['url'];
    $wid_basketball_title = $wid_basketball['title'];
    $wid_basketball_alt = $wid_basketball['alt'];
    $wid_basketball_caption = $wid_basketball['caption'];
    ?>
    <div class="wid-basketball-section">
        <img src="<?php echo $wid_basketball_url; ?>" alt="<?php echo $wid_basketball_alt; ?>" class="hidden img-basketball img-responsive" data-rjs="2"/>
    </div>
<?php endif; ?>

<?php
$basketball_hoop_image = get_field('basketball_hoop_image');

if( !empty($basketball_hoop_image) ):
    // vars
    $bhi_url = $basketball_hoop_image['url'];
    $bhi_title = $basketball_hoop_image['title'];
    $bhi__alt = $basketball_hoop_image['alt'];
    $bhi_caption = $basketball_hoop_image['caption'];
    ?>
    <div class="wid-img bhi fm-featured-img">
        <img src="<?php echo $bhi_url; ?>" alt="<?php echo $bhi__alt; ?>" class="img-responsive" />
    </div>
<?php endif; ?>
