<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */
?>



<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
	</div><!-- Close off-canvas content -->
<?php endif; ?>

<?php wp_footer(); ?>
<script type="text/javascript" src="/wp-content/themes/franklin-theme/dist/assets/js/jquery.preload.min.js"></script>
<script type="text/javascript" >

 $( function(){
	 <?php if( have_rows('home_page_repeater','options') ): ?>
	 	 $.preload([
	 	<?php while ( have_rows('home_page_repeater', 'options') ) : the_row(); ?>
	 		<?php
	 		$image_url = get_sub_field('po_background_image', 'options');
	 		if( !empty($image_url) ):
	 			echo "'".$image_url['url']."',\r\n";
	 		endif;

	 		?>
	 	<?php endwhile;

		?>
	 ]);
	<?php endif; ?>
 });

</script>
</body>
</html>
