<?php

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Theme Options',
		'menu_title'	=> 'Theme Options',
		'menu_slug' 	=> 'theme-general-options',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Front Page',
		'menu_title'	=> 'Front Page',
		'parent_slug'	=> 'theme-general-options',
	));
    acf_add_options_sub_page(array(
		'page_title' 	=> 'Social Settings',
		'menu_title'	=> 'Social',
		'parent_slug'	=> 'theme-general-options',
	));
    acf_add_options_sub_page(array(
        'page_title' 	=> 'Photo Gallery',
        'menu_title'	=> 'Photo Gallery',
        'parent_slug'	=> 'theme-general-options',
    ));

    // acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Social Settings',
	// 	'menu_title'	=> 'Social',
	// 	//'parent_slug'	=> 'theme-general-options',
	// ));
	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Theme Footer Settings',
	// 	'menu_title'	=> 'Footer',
	// 	'parent_slug'	=> 'theme-general-settings',
	// ));

}
