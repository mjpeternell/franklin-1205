<?php
if (!function_exists( 'post_nav')):
/**
 * Display navigation to next/previous pages when applicable
 */
function post_nav($nav_id) {
	global $wp_query, $post;

	// Don't print empty markup on single pages if there's nowhere to navigate.
	if ( is_single() ) {
		$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
		$next = get_adjacent_post( false, '', false );

		if ( ! $next && ! $previous )
			return;
	}

	// Don't print empty markup in archives if there's only one page.
	if ( $wp_query->max_num_pages < 2 && ( is_home() || is_archive() || is_search() ) )
		return;

	$nav_class = ( is_single() ) ? 'post-navigation' : 'paging-navigation';

	?>
	<nav role="navigation" id="<?php echo esc_attr( $nav_id ); ?>" class="<?php echo $nav_class; ?>">
		<h4 class="screen-reader-text"><?php _e( 'Post navigation', 'foundationpress' ); ?></h4>

	<?php if ( is_single() ) : // navigation links for single posts ?>

		<div class="grid-x">
			<div class="cell medium-6 large-6">
				<?php previous_post_link( '<div class="nav-previous">%link</div>', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'foundationpress' ) . '</span> %title' ); ?>
			</div><!-- .col-md-4 -->
			<div class="cell medium-6 large-6 col-nav-next">
				<?php next_post_link( '<div class="nav-next">%link</div>', '%title <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'foundationpress' ) . '</span>' ); ?>
			</div><!-- .col-md-4 -->
		</div><!-- .row -->

	<?php elseif ($wp_query->max_num_pages > 1 && (is_home() || is_archive() || is_search())) : // navigation links for home, archive, and search pages ?>
		<div class="grid-x">
			<div class="cell medium-6 large-6">
				<?php if (get_next_posts_link()) : ?>
				<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'foundationpress' ) ); ?></div>
				<?php endif; ?>

			</div><!-- .col-md-4 -->
			<div class="cell medium-6 large-6 col-nav-next">
				<?php if (get_previous_posts_link()) : ?>
				<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'foundationpress' ) ); ?></div>
				<?php endif; ?>

			</div><!-- .col-md-4 -->
		</div><!-- .row -->

	<?php endif; ?>

	</nav><!-- #<?php echo esc_html( $nav_id ); ?> -->
	<?php
}
endif; // upbootwp_content_nav
