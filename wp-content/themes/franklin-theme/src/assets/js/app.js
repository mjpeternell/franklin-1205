import $ from 'jquery';

import whatInput from 'what-input';

import 'retinajs/dist/retina.min';
import 'slick-carousel/slick/slick.min.js'

//Animation stuff
import 'textillate/jquery.textillate.js';
import 'fittext/dist/jquery.fittext.js';
import 'letteringjs/jquery.lettering.js'
import 'velocity-animate/velocity.min.js';
import 'velocity-animate/velocity.ui.min.js';
import 'jquery-scrollify/jquery.scrollify.js';


//Parallax stuff
import 'jquery-parallax.js/parallax.min';

window.$ = $;

import Foundation from 'foundation-sites';

//ANIMATE ON SCROLL
//import AOS from 'aos';

// animejs
import anime from 'animejs/lib/anime.es.js';
// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';

$(document).foundation();

$(window).on('load', function(){
    function preload(arrayOfImages) {
        $(arrayOfImages).each(function(){
            $('<img/>')[0].src = this;
            // Alternatively you could use:
            //(new Image()).src = this;
        });
    }
});


// Resize Event Handling
var rtime;
var timeout = false;
var delta = 500;
$(window).on('resize', function() {
    rtime = new Date();
    // $.scrollify.update();
    if (!timeout) {
        timeout = true;
        setTimeout(resizeend, delta);
    }
});

function resizeend() {
    const sectionname = $.scrollify.current();
    setTimeout( function () {
        timeout = false;
        $.scrollify.instantMove('#' + ($(sectionname[0]).index() + 1));
    }, 500);
}

$( document ).ready(function() {
    $.scrollify({
        section : "section.vs-section-a",
    });

    //Force window reload to help with styling.
    $(window).resize(function() {
        location.reload();
    });




    $('#home .learn-more').click(function(){
        $.scrollify.next();
    });

    $('#menu-main-nav li a').click(function(e) {
        // console.log('---',  $('#menu-main-nav li a').index(this));
        var index = $('#menu-main-nav li a').index(this);
        $.scrollify.instantMove('/#' + (index + 1));
        $('.off-canvas').foundation('close');
    });

    $(document).on('open.zf.reveal', '#galleryModal[data-reveal]', function() {
      var $modal = $(this);
      //console.log($modal);
    });

    $('#' + sectionIds[0] + ' .fm-featured-img').css('display', 'block');
    verticalFeaturedImageAnimation('#' + sectionIds[0] + ' .fm-featured-img');
    animationStarted['#' + sectionIds[0]] = true;

    // AOS.init({
    //     disable: 'mobile',
    //     // duration: 800,
    //     //mirror: false,
    //     // //anchorPlacement: 'top-top',
    //     //offset: -200
    // });

    // jQuery('#home h1').textillate({
    //     initialDelay: 0,
    //     in: {
    //         effect:     'flipInY',
    //         delay:      50,
    //     }
    // }).fitText(0.5);

    // setTimeout(function () {
    //   jQuery('h1.glow').removeClass('in');
    // }, 2000);


    // Home Title Animation
    $('#home h1').removeClass('hidden');

    $('#home h1').each(function(){
         $(this).html($(this).text().replace(/\S/g, "<span class='letter'>$&</span>"));
    });
    var homeTitleAnimeDelay = 1200;
    anime({
        targets: '#home h1 .letter',
        opacity: [0,1],
        translateZ: 0,
        easing: "easeOutExpo",
        duration: 20,
        endDelay: 10,
        delay: function(el, i) {
        switch (i) {
            case 3: homeTitleAnimeDelay += 120;
            case 25: homeTitleAnimeDelay += 120;
            case 28: homeTitleAnimeDelay += 300;
            default: homeTitleAnimeDelay += 20;
        }
        return homeTitleAnimeDelay;
        },
        complete: function () {
            $('#home .description').removeClass('hidden');
            anime({
                targets: '#home .description',
                opacity: [0,1],
                translateY: [400, 0],
                easing: "easeOutExpo",
                duration: 2000,
                delay: 300
            });
        }
    });

    $('#photoBtn').click(function(e){

    });

    // $('.parallax-window').parallax({
    //     naturalWidth: 1440,
    //     naturalHeight: 980,
    //     speed: 0.75
    // });

    $('.home #menu-main-nav li a[href*="#"]:not([href="#"])').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        // set target to ancho
        if (window.location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && window.location.hostname === this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // console.log(target);
            if (target.length) {

                // scroll to each target
                $(target).velocity('scroll', {
                    duration: 800,
                    offset: 0,
                    easing: 'easeInCubic'
                });
            }
        }
    });



    // *only* if we have anchor on the url
    if (window.location.hash) {
        $(window.location.hash).velocity('scroll', {
            duration: 800,
            offset: 0,
            easing: 'easeInSine'
        });
    }

});

retinajs();



// Show V-Log Modal
function showVlogModal() {
    $.scrollify.disable();
    console.log('show v log');
    $('#vlogModal').addClass('show-reveal-modal');
    $('#vlogModal  .wrapper .anime-bar').removeClass('hidden');
    var timeline = anime.timeline();
    timeline
        .add({
            targets: '#vlogModal .wrapper .anime-bar',
            scaleY: [0, 0.2],
            easing: "linear",
            duration: 400,
        })
        .add({
            targets: '#vlogModal .wrapper .anime-bar',
            scaleY: [0.2, 0.1],
            easing: "linear",
            duration: 400,
        })
        .add({
            targets: '#vlogModal .wrapper .anime-bar',
            scaleY: [0.1, 1],
            easing: "linear",
            duration: 800,
            complete: function() {
                $('#vlogModal .content .back-link').removeClass('hidden');
                $('#vlogModal .content .close-button').removeClass('hidden');
            }
        })
        .add({
            targets: '#vlogModal .content .page-title',
            scaleY: [0, 0],
            easing: "linear",
            duration: 800,
            complete: function() {
                $('#vlogModal .content .page-title').removeClass('hidden');
            }
        })
        .add({
            targets: '#vlogModal .content .page-title',
            scaleY: [0.8, 1],
            easing: "easeOutExpo",
            duration: 300,
            endDelay: 500,
            complete: function() {
                $('#vlogModal .content .vpost').removeClass('hidden');
            }
        })
        .add({
            targets: '#vlogModal .content .vpost',
            opacity: [0, 1],
            easing: "linear",
            duration: 800,
            delay: function(el, i) {
                var columns = 3;
                if (window.innerWidth > 1024) columns = 3;
                if (window.innerWidth < 1024) columns = 2;
                if (window.innerWidth < 640) columns = 1;

                return 800 * (i % columns);
            },
            complete: function () {
                $('#vlogModal  .content').css('background', '#000');
            }
        })


}
var vlogModalBtn = document.querySelector('#vlogModalBtn');
vlogModalBtn.addEventListener('click', showVlogModal);

function hideVlogModal() {
    $.scrollify.enable();
    anime.remove('#vlogModal .content .vpost');
    $('#vlogModal  .content').css('background', 'transparent');
    var timeline = anime.timeline();
    timeline
        .add({
            targets: '#vlogModal .content .vpost, #vlogModal .content .back-link, #vlogModal .content .close-button, #vlogModal .content .page-title',
            opacity: [1, 0],
            easing: "linear",
            duration: 500
        })
        .add({
            targets: '#vlogModal .wrapper .anime-bar',
            scaleY: [1, 0.8],
            easing: "linear",
            duration: 400,
        })
        .add({
            targets: '#vlogModal .wrapper .anime-bar',
            scaleY: [0.8, 0.9],
            easing: "linear",
            duration: 400,
        })
        .add({
            targets: '#vlogModal .wrapper .anime-bar, #vlogModal .content .vpost',
            scaleY: [0.9, 0],
            easing: "linear",
            duration: 800,
            complete: function () {
                $('#vlogModal  .wrapper .anime-bar').addClass('hidden');
                $('#vlogModal').removeClass('show-reveal-modal');
                $('#vlogModal .content .back-link').addClass('hidden');
                $('#vlogModal .content .close-button').addClass('hidden');
                $('#vlogModal .content .page-title').addClass('hidden');
                $('#vlogModal .content .vpost').addClass('hidden');
                $('#vlogModal .content .vpost').css({'opacity': 1, 'transform': 'scaleY(1)'});
                $('#vlogModal .content .vpost, #vlogModal .content .back-link, #vlogModal .content .close-button, #vlogModal .content .page-title').css('opacity', 1);
                // $.scrollify.enable();
            }
        })
}
var hideVlogModalBtn = document.querySelector('#vlogModal .content .close-button');
hideVlogModalBtn.addEventListener('click', hideVlogModal);


function showWiaCircleModal() {
    $('.showsBtn.circular-modalbtn-wrapper .outer-circle').removeClass('hidden');
    $('.showsBtn.circular-modalbtn-wrapper .inner-circle').removeClass('hidden');
    $('.showsBtn.circular-modalbtn-wrapper .image').removeClass('hidden');
    $('.showsBtn.circular-modalbtn-wrapper .my-show-btn').addClass('hidden');
    $.scrollify.disable();

    var wWidth = $(window).width();
    anime({
        targets: '.showsBtn.circular-modalbtn-wrapper .outer-circle',
        scale: [1, wWidth/100 * 3],
        easing: "linear",
        duration: 1200,
    });

    anime({
        targets: '.showsBtn.circular-modalbtn-wrapper .image',
        scale: [1, 6],
        translateX: [0, -wWidth/25],
        easing: "linear",
        duration: 800,
        delay: 200,
        complete: function() {
            $('#showsModal').addClass('without-overlay');
            $('#showsModal').css('display', 'block');
            $('.site-header').hide();
            $('button.menu-icon').hide();
            anime({
                targets: '#showsModal .grid-container',
                scale: [0.7, 1],
                opacity: [0, 1],
                easing: "linear",
                duration: 500,
            })
            $('body').addClass('stop-scrolling');

        }
    });
    anime({
        targets: '.showsBtn.circular-modalbtn-wrapper .inner-circle',
        scale: [1, wWidth/100 * 2],
        easing: "linear",
        duration: 1800,
        delay: 100,
    });
}
// scale(15) translateX(15px) translateY(-35%)
var showWiaCircleModalBtn = document.querySelector('#showsBtn');
showWiaCircleModalBtn.addEventListener('click', showWiaCircleModal);


function hideWiaCircleModal() {
    var wWidth = $(window).width();

    anime({
        targets: '#showsModal .grid-container',
        scale: [1, 0.7],
        opacity: [1, 0.5],
        easing: "linear",
        duration: 800,
        complete: function () {
            $('#showsModal').removeClass('without-overlay');
            $('#showsModal').css('display', 'none');
            anime({
                targets: '.showsBtn.circular-modalbtn-wrapper .image',
                scale: [6, 1],
                translateX: [0, 0],
                easing: "linear",
                duration: 500,
            });
        }
    })
    anime({
        targets: '.showsBtn.circular-modalbtn-wrapper .outer-circle',
        scale: [wWidth/100 * 2, 1],
        easing: "linear",
        duration: 1500,
        delay: 100,
        complete: function() {
            $.scrollify.enable();
            $('body').removeClass('stop-scrolling');
            $('.showsBtn.circular-modalbtn-wrapper .outer-circle').addClass('hidden');
            $('.showsBtn.circular-modalbtn-wrapper .inner-circle').addClass('hidden');
            $('.showsBtn.circular-modalbtn-wrapper .image').addClass('hidden');
            $('.showsBtn.circular-modalbtn-wrapper .my-show-btn').removeClass('hidden');
            $('button.menu-icon').show();
            $('.site-header').show();
        }
    });

    anime({
        targets: '.showsBtn.circular-modalbtn-wrapper .inner-circle',
        scale: [wWidth/100 * 2, 1],
        easing: "linear",
        duration: 1400,
    });
}

var hideWiaCircleModalBtn = document.querySelector('#showsModal .close-button');
hideWiaCircleModalBtn.addEventListener('click', hideWiaCircleModal);

// Booke ME Animation
function showBMCircleModal() {
    $('.showsBtn.circular-modalbtn-wrapper .outer-circle').removeClass('hidden');
    $('.showsBtn.circular-modalbtn-wrapper .inner-circle').removeClass('hidden');
    $('.showsBtn.circular-modalbtn-wrapper .my-show-btn').addClass('hidden');
    $.scrollify.disable();

    var wWidth = $(window).width();
    anime({
        targets: '.showsBtn.circular-modalbtn-wrapper .outer-circle',
        scale: [1, wWidth/100 * 2],
        easing: "linear",
        duration: 1200,
    });
    anime({
        targets: '.showsBtn.circular-modalbtn-wrapper .inner-circle',
        scale: [1, wWidth/100 * 2],
        easing: "linear",
        duration: 1300,
        delay: 100,
        complete: function() {
            $('#exampleModal8').addClass('without-overlay');
            $('#exampleModal8').css('display', 'block');
            $('.wpcf7-form-control.wpcf7-submit').css('visibility', 'hidden');
            anime({
                targets: '#exampleModal8 .grid-container',
                scale: [0.7, 1],
                opacity: [0, 1],
                easing: "linear",
                duration: 500,
                endDelay: 300,
                complete: function() {
                    $('.wpcf7-form-control.wpcf7-submit').css('visibility', 'visible');
                    anime({
                        targets: '.wpcf7-form-control.wpcf7-submit',
                        scale: [0, 1.1],
                        opacity: [0, 1],
                        easing: "linear",
                        duration: 800,
                    });
                    $('body').addClass('stop-scrolling');

                }

            })
        }
    });
}
var showBMCircleModalBtn = document.querySelector('#bookMeBtn');
showBMCircleModalBtn.addEventListener('click', showBMCircleModal);


function hideBMCircleModal() {
    var wWidth = $(window).width();
    $('body').removeClass('stop-scrolling');
    anime({
        targets: '.wpcf7-form-control.wpcf7-submit',
        scale: [1.1, 0],
        opacity: [1, 0],
        easing: "linear",
        duration: 500,
        complete: function () {
            anime({
                targets: '#exampleModal8 .grid-container',
                scale: [1, 0.7],
                opacity: [1, 0.5],
                easing: "linear",
                duration: 700,
                complete: function () {
                    $('#exampleModal8').removeClass('without-overlay');
                    $('#exampleModal8').css('display', 'none');
                }
            })
        }
    });

    anime({
        targets: '.showsBtn.circular-modalbtn-wrapper .outer-circle',
        scale: [wWidth/100 * 2, 1],
        easing: "linear",
        duration: 1300,
        delay: 900,
        complete: function() {
            $.scrollify.enable();
            $('.showsBtn.circular-modalbtn-wrapper .outer-circle').addClass('hidden');
            $('.showsBtn.circular-modalbtn-wrapper .inner-circle').addClass('hidden');
            $('.showsBtn.circular-modalbtn-wrapper .my-show-btn').removeClass('hidden');
        }
    });

    anime({
        targets: '.showsBtn.circular-modalbtn-wrapper .inner-circle',
        scale: [wWidth/100 * 2, 1],
        easing: "linear",
        duration: 1200,
        delay: 800,
    });
}

var hideBMCircleModalBtn = document.querySelector('#exampleModal8 .close-button');
hideBMCircleModalBtn.addEventListener('click', hideBMCircleModal);


// My Experiences Animation
function showMECircleModal() {
    $('.photoBtn.circular-modalbtn-wrapper .outer-circle').removeClass('hidden');
    $('.photoBtn.circular-modalbtn-wrapper .inner-circle').removeClass('hidden');
    $('.photoBtn.circular-modalbtn-wrapper .my-show-btn').addClass('hidden');
    $.scrollify.disable();

    var wWidth = $(window).width();
    anime({
        targets: '.photoBtn.circular-modalbtn-wrapper .outer-circle',
        scale: [1, wWidth/100 * 2],
        easing: "linear",
        duration: 1200,
    });

    anime({
        targets: '.photoBtn.circular-modalbtn-wrapper .inner-circle',
        scale: [1, wWidth/100 * 2],
        easing: "linear",
        duration: 1400,
        delay: 100,
        complete: function() {
            $('#galleryModal').addClass('without-overlay');
            $('#galleryModal').css({'background': '#BF1E2E', 'display': 'block' });
            $('body').addClass('stop-scrolling');

            // Slick Gallery
            $('.gallery-reveal .slick-gallery').not('.slick-initialized').slick({
                arrows: false,
                asNavFor: '.slider-thumb',
                centerPadding: 0,
                dots: false,
                fade: true,
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,

            });
            $('.gallery-reveal .slider-thumb').not('.slick-initialized').slick({
                adaptiveHeight: true,
                arrows: true,
                asNavFor: '.slick-gallery',
                slidesToShow: 6,
                slidesToScroll: 1,
                centerMode: true,
                dots: false,
                focusOnSelect: true,

                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            arrows: true,
                            centerMode: true,
                            centerPadding: '16px',
                            slidesToShow: 3,
                            slidesToScroll: 1,
                            adaptiveHeight: true,

                        }
                    }
                ]
                // responsive: [
                //     {
                //         breakpoint: 768,
                //         settings: {
                //             arrows: false,
                //             adaptiveHeight: true,
                //             centerMode: true,
                //             centerPadding: '0px',
                //         },
                // ]
            });
            anime({
                targets: '#galleryModal .grid-container',
                scale: [0.7, 1],
                opacity: [0, 1],
                easing: "linear",
                duration: 500,
            })
        }
    });
}
// scale(15) translateX(15px) translateY(-35%)
var showMECircleModalBtn = document.querySelector('#photoBtn');
showMECircleModalBtn.addEventListener('click', showMECircleModal);


function hideMECircleModal() {
    var wWidth = $(window).width();
    $('#galleryModal').css({'background': 'transparent'});

    anime({
        targets: '#galleryModal .grid-container',
        scale: [1, 0.5],
        opacity: [1, 0],
        easing: "linear",
        duration: 600,
    });
    anime({
        targets: '.photoBtn.circular-modalbtn-wrapper .outer-circle',
        scale: [wWidth/100 * 2, 1],
        easing: "linear",
        duration: 1500,
        delay: 100,
        complete: function () {
            $('#galleryModal').removeClass('without-overlay');
            $('#galleryModal').css({'display':'none'});
            $.scrollify.enable();
            $('body').removeClass('stop-scrolling');
            $('.photoBtn.circular-modalbtn-wrapper .outer-circle').addClass('hidden');
            $('.photoBtn.circular-modalbtn-wrapper .inner-circle').addClass('hidden');
            $('.photoBtn.circular-modalbtn-wrapper .image').addClass('hidden');
            $('.photoBtn.circular-modalbtn-wrapper .my-show-btn').removeClass('hidden');
        }
    });

    anime({
        targets: '.photoBtn.circular-modalbtn-wrapper .inner-circle',
        scale: [wWidth/100 * 2, 1],
        easing: "linear",
        duration: 1400,
    });
}

var hideMECircleModalBtn = document.querySelector('#galleryModal .close-button');
hideMECircleModalBtn.addEventListener('click', hideMECircleModal);


var animationStarted = {};
var originWiaTop = {};
var sectionIds = [
    'home',
    "who-i-am",
    'what-i-am-about',
    'my-experiences',
    'book-me',
    'wear-me',
    'contact-me',
    'what-i-do'
];

// init basketball animation

var positionY = 2000,
    positionX = -50,
    rotate = 0,
    stepX = $(window).width() * 0.83,
    rangeY = 250,
    bouncingRate = .5,
    rotateAngle = 200,
    ballAnimationStarted = false;

var lastScrollTop = 0;
var directionDown = false;
var followMeSection = false;
var whoIamSection = false;
var whatIdoSection = false;
var whatIamAboutSection = false;
var myExperiencesSection = false;
var bookMeSection = false;
var wearMeSection = false;

$(window).scroll(function() {
    var currentScrollTop = $(this).scrollTop();
    if (currentScrollTop > lastScrollTop) {
        directionDown = true;
    } else {
        directionDown = false;
    }
    lastScrollTop = currentScrollTop;

    var widTop = $('#what-i-do').offset().top,
        homeTop = $('#home').offset().top,
        whoiamTop = $('#who-i-am').offset().top,
        wiaaTop = $('#what-i-am-about').offset().top,
        meTop = $('#my-experiences').offset().top,
        bmTop = $('#book-me').offset().top,
        fmTop = $('#follow-me').offset().top,
        wmTop = $('#wear-me').offset().top,
        cmTop = $('#contact-me').offset().top,
        sectionHeight = $('#what-i-do').outerHeight(),
        wH = $(window).height(),
        wW = $(window).width();

    if ((currentScrollTop > widTop - 50) && !ballAnimationStarted) {
        ballAnimationStarted = true;
        $('.wid-basketball-section img').addClass('hidden');
        anime.remove('.wid-basketball-section img');
        setTimeout(function() {
            var animationLoop = setInterval(function() {
                if (positionX < -wW * 1.35 - 200) {
                  clearInterval(animationLoop);
                  $('.wid-basketball-section img').addClass('hidden');
                } else {
                    console.log('position y:', positionY);
                    if (positionY < 200) {
                    positionY = 0;
                    stepX = 200;
                    rotateAngle = 40;
                    } else {
                    stepX = stepX * .6;
                    positionY = positionY * bouncingRate;

                    }
                    rotate = rotate + rotateAngle;
                    startBallAnimation(positionX, positionY, rangeY, stepX, rotate, bouncingRate, rotateAngle);
                    positionX = positionX - stepX;
                }
            }, 700);
        }, 500);

    }
    if (currentScrollTop <= widTop - 50) {
        ballAnimationStarted = false;
        anime.remove('.img-basketball');
        positionY = 2000;
        positionX = -50;
        rotate = 0;
        stepX = $(window).width() * 0.83;
        rangeY = 250;
        bouncingRate = .5;
        rotateAngle = 200;
        $('.wid-basketball-section img').addClass('hidden');
    }

    // Follow Me
    var follow_me_tl = anime.timeline();
    if (currentScrollTop > fmTop - 50 && !followMeSection) {
        followMeSection = true;
        $('#follow-me .fm-featured-img').css('display', 'block');
        $('#follow-me .content-inner h1').removeClass('hidden');
        $('#follow-me .content-inner .description').removeClass('hidden');
        follow_me_tl
        .add({
            targets: '#follow-me .content-inner h1',
            opacity: [0,1],
            translateY: [200, 0],
            easing: "easeInOutSine",
            duration: 300,
        })
        .add({
            targets: '#follow-me .content-inner .description',
            opacity: [0,1],
            translateY: [200, 0],
            easing: "easeInOutSine",
            duration: 300,
            endDelay: 300,
            complete: function() {
                $('#follow-me .content-inner .dashes .dividers').removeClass('hidden');
            }
        })
        .add({
            targets: '#follow-me .fm-featured-img',
            opacity: [0,1],
            scale: [0, 1],
            rotate: [0, 6],
            duration: 1200,
            delay: 200,
            easing: 'easeOutExpo',
            complete: function() {
                $('#follow-me .social-links').removeClass('hidden');
            }
        })
        .add({
            targets: '#follow-me .social-link a',
            opacity: [0,1],
            scale: [0, 1],
            easing: 'cubicBezier(.5, .05, .1, .3)',
            duration: 600,
            delay: anime.stagger(300, {start: 100}),
            complete: function() {
                anime({
                    targets: '#follow-me .fm-featured-img',
                    rotate: [6, -6],
                    loop: true,
                    direction: 'alternate',
                    easing: 'easeInOutSine',
                    duration: 500,
                    delay: 1000,
                    endDelay: 0,
                })

            }
        });
    }
    if (currentScrollTop <= fmTop - 50) {
        followMeSection = false;
        $('#follow-me .social-links').addClass('hidden');
        $('#follow-me .fm-featured-img').css('display', 'none');
        anime.remove('#follow-me .fm-featured-img');
        $('#follow-me .content-inner h1').addClass('hidden');
        $('#follow-me .content-inner .description').addClass('hidden');
    }

    // who-i-am Section Animations
    var whoIamTl = anime.timeline();
    if (currentScrollTop > whoiamTop - 50 && !whoIamSection) {
        whoIamSection = true;
        $('#who-i-am .content-inner h1').removeClass('hidden');
        $('#who-i-am .description').removeClass('hidden');
        whoIamTl
        .add({
            targets: '#who-i-am .content-inner h1',
            opacity: [0,1],
            translateY: [200, 0],
            easing: "easeInOutSine",
            duration: 300,
        })
        .add({
            targets: '#who-i-am .content-inner .description',
            opacity: [0,1],
            translateY: [200, 0],
            easing: "easeInOutSine",
            duration: 500,
            endDelay: 300,
            complete: function() {
                $('#who-i-am .content-inner .dashes .dividers').removeClass('hidden');
            }
        })
        .add({
            targets: '#who-i-am .content-inner .dashes .dividers',
            opacity: [0,1],
            easing: "easeOutExpo",
            duration: 700,
            endDelay: 100,
            complete: function() {
                $('#who-i-am .content-inner .dashes .dash-btn').removeClass('hidden');
            }
        })
        .add({
            targets: '#who-i-am .content-inner .dashes .dash-btn',
            translateY: [20, 0],
            opacity: [0,1],
            easing: "linear",
            duration: 200,
            endDelay: 200,
            complete: function() {
                $('#who-i-am .content-inner .dashes .my-show-btn').removeClass('hidden');
            }
        })
        .add({
            targets: '#who-i-am .content-inner .dashes .my-show-btn',
            scale: [0, 1],
            easing: "linear",
            duration: 600,
            endDelay: 2000,
            complete: function () {
                anime({
                    targets: '#who-i-am .content-inner .dashes .my-show-btn',
                    scale: [1, 1.1],
                    easing: "linear",
                    direction: 'alternate',
                    loop: true,
                    duration: 300,
                    endDelay: 0,
                    delay: 1000,
                })
            }
        });
    }
    if (currentScrollTop <= whoiamTop -50) {
        whoIamSection = false;
        $('#who-i-am .content-inner .dash-btn').addClass('hidden');
        $('#who-i-am .content-inner .my-show-btn').addClass('hidden');
        $('#who-i-am .content-inner .dashes .dividers').addClass('hidden');
        $('#who-i-am .content-inner .description').addClass('hidden');
        $('#who-i-am .content-inner h1').addClass('hidden');
        anime.remove('#who-i-am .content-inner .dashes .my-show-btn');
    }

    // what-i-do Section Animations
    var whatIdoTl = anime.timeline();
    if (currentScrollTop > widTop - 50 && !whatIdoSection) {
        whatIdoSection = true;
        $('#what-i-do .content-inner h1').removeClass('hidden');
        $('#what-i-do .description').removeClass('hidden');
        whatIdoTl
        .add({
            targets: '#what-i-do .content-inner h1',
            translateY: [200, 0],
            easing: "easeInOutSine",
            duration: 300,
        })
        .add({
            targets: '#what-i-do .content-inner .description',
            opacity: [0,1],
            translateY: [300, 0],
            easing: "linear",
            duration: 400,
        })
    }
    if (currentScrollTop <= widTop -50) {
        whatIdoSection = false;
        $('#what-i-do .content-inner .description').addClass('hidden');
        $('#what-i-do .content-inner h1').addClass('hidden');
        // anime.remove();
    }

    // What-I-am-about Section Animations
    var whatIamAboutTl = anime.timeline();
    if (currentScrollTop > wiaaTop - 50 && !whatIamAboutSection) {
        whatIamAboutSection = true;
        $('#what-i-am-about .content-inner h1').removeClass('hidden');
        $('#what-i-am-about .description').removeClass('hidden');
        whatIamAboutTl
        .add({
            targets: '#what-i-am-about .content-inner h1',
            translateY: [200, 0],
            easing: "easeInOutSine",
            duration: 300,
        })
        .add({
            targets: '#what-i-am-about .content-inner .description',
            opacity: [0,1],
            translateY: [300, 0],
            easing: "linear",
            duration: 400,
            endDelay: 300,
            complete: function() {
                $('#what-i-am-about .content-inner .dashes .dividers').removeClass('hidden');
            }
        })
        .add({
            targets: '#what-i-am-about .content-inner .dashes .dividers',
            opacity: [0,1],
            easing: "easeOutExpo",
            duration: 700,
            endDelay: 100,
            complete: function() {
                $('#what-i-am-about .content-inner .dashes .dash-btn').removeClass('hidden');
            }
        })
        .add({
            targets: '#what-i-am-about .content-inner .dashes .dash-btn',
            translateY: [20, 0],
            opacity: [0,1],
            easing: "linear",
            duration: 200,
            endDelay: 200,
            complete: function() {
                $('#what-i-am-about .content-inner .dashes .my-show-btn').removeClass('hidden');
            }
        })
        .add({
            targets: '#what-i-am-about .content-inner .dashes .my-show-btn',
            scale: [0, 1],
            easing: "linear",
            duration: 600,
            endDelay: 2000,
            complete: function () {
                anime({
                    targets: '#what-i-am-about .content-inner .dashes .my-show-btn',
                    scale: [1, 1.1],
                    easing: "linear",
                    direction: 'alternate',
                    loop: true,
                    duration: 300,
                    endDelay: 0,
                    delay: 1000,
                })
            }
        });
    }
    if (currentScrollTop <= wiaaTop -50) {
        whatIamAboutSection = false;
        $('#what-i-am-about .content-inner .dash-btn').addClass('hidden');
        $('#what-i-am-about .content-inner .my-show-btn').addClass('hidden');
        $('#what-i-am-about .content-inner .dashes .dividers').addClass('hidden');
        $('#what-i-am-about .content-inner .description').addClass('hidden');
        $('#what-i-am-about .content-inner h1').addClass('hidden');
        anime.remove('#what-i-am-about .content-inner .dashes .my-show-btn');
    }

    // my-experiences section animation
    var myExperienceTl = anime.timeline();
    if (currentScrollTop > meTop - 50 && !myExperiencesSection) {
        myExperiencesSection = true;
        $('#my-experiences .content-inner h1').removeClass('hidden');
        $('#my-experiences .description').removeClass('hidden');
        myExperienceTl
        .add({
            targets: '#my-experiences .content-inner h1',
            translateY: [200, 0],
            easing: "easeInOutSine",
            duration: 300,
        })
        .add({
            targets: '#my-experiences .content-inner .description',
            opacity: [0,1],
            translateY: [300, 0],
            easing: "linear",
            duration: 400,
            endDelay: 300,
            complete: function() {
                $('#my-experiences .content-inner .dashes .dividers').removeClass('hidden');
            }
        })
        .add({
            targets: '#my-experiences .content-inner .dashes .dividers',
            opacity: [0,1],
            easing: "easeOutExpo",
            duration: 700,
            endDelay: 100,
            complete: function() {
                $('#my-experiences .content-inner .dashes .dash-btn').removeClass('hidden');
            }
        })
        .add({
            targets: '#my-experiences .content-inner .dashes .dash-btn',
            translateY: [20, 0],
            opacity: [0,1],
            easing: "linear",
            duration: 200,
            endDelay: 200,
            complete: function() {
                $('#my-experiences .content-inner .dashes .my-show-btn').removeClass('hidden');
            }
        })
        .add({
            targets: '#my-experiences .content-inner .dashes .my-show-btn',
            scale: [0, 1],
            easing: "linear",
            duration: 600,
            endDelay: 2000,
            complete: function () {
                anime({
                    targets: '#my-experiences .content-inner .dashes .my-show-btn',
                    scale: [1, 1.1],
                    easing: "linear",
                    direction: 'alternate',
                    loop: true,
                    duration: 300,
                    endDelay: 0,
                    delay: 1000,
                })
            }
        });
    }
    if (currentScrollTop <= meTop -50) {
        myExperiencesSection = false;
        $('#my-experiences .content-inner .dash-btn').addClass('hidden');
        $('#my-experiences .content-inner .my-show-btn').addClass('hidden');
        $('#my-experiences .content-inner .dashes .dividers').addClass('hidden');
        $('#my-experiences .content-inner .description').addClass('hidden');
        $('#my-experiences .content-inner h1').addClass('hidden');
        anime.remove('#my-experiences .content-inner .dashes .my-show-btn');
    }

    // Book me
    var bookMeTl = anime.timeline();
    if (currentScrollTop > bmTop - 50 && !bookMeSection) {
        bookMeSection = true;
        $('#book-me .content-inner h1').removeClass('hidden');
        $('#book-me .description').removeClass('hidden');
        bookMeTl
        .add({
            targets: '#book-me .content-inner h1',
            translateY: [200, 0],
            easing: "easeInOutSine",
            duration: 300,
        })
        .add({
            targets: '#book-me .content-inner .description',
            opacity: [0,1],
            translateY: [300, 0],
            easing: "linear",
            duration: 400,
            endDelay: 300,
            complete: function() {
                $('#book-me .content-inner .dashes .dividers').removeClass('hidden');
            }
        })
        .add({
            targets: '#book-me .content-inner .dashes .dividers',
            opacity: [0,1],
            easing: "easeOutExpo",
            duration: 700,
            endDelay: 100,
            complete: function() {
                $('#book-me .content-inner .dashes .dash-btn').removeClass('hidden');
            }
        })
        .add({
            targets: '#book-me .content-inner .dashes .dash-btn',
            translateY: [20, 0],
            opacity: [0,1],
            easing: "linear",
            duration: 200,
            endDelay: 200,
            complete: function() {
                $('#book-me .content-inner .dashes .my-show-btn').removeClass('hidden');
            }
        })
        .add({
            targets: '#book-me .content-inner .dashes .my-show-btn',
            scale: [0, 1],
            easing: "linear",
            duration: 600,
            endDelay: 2000,
            complete: function () {
                anime({
                    targets: '#book-me .content-inner .dashes .my-show-btn',
                    scale: [1, 1.1],
                    easing: "linear",
                    direction: 'alternate',
                    loop: true,
                    duration: 300,
                    endDelay: 0,
                    delay: 1000,
                })
            }
        });
    }
    if (currentScrollTop <= meTop -50) {
        bookMeSection = false;
        $('#book-me .content-inner .dash-btn').addClass('hidden');
        $('#book-me .content-inner .my-show-btn').addClass('hidden');
        $('#book-me .content-inner .dashes .dividers').addClass('hidden');
        $('#book-me .content-inner .description').addClass('hidden');
        $('#book-me .content-inner h1').addClass('hidden');
        anime.remove('#book-me .content-inner .dashes .my-show-btn');
    }

    // Wear Me animation
    var wearMeTl = anime.timeline();
    if (currentScrollTop > wmTop - 50 && !wearMeSection) {
        wearMeSection = true;
        $('#wear-me .content-inner h1').removeClass('hidden');
        $('#wear-me .description').removeClass('hidden');
        wearMeTl
        .add({
            targets: '#wear-me .content-inner h1',
            translateY: [200, 0],
            easing: "easeInOutSine",
            duration: 300,
        })
        .add({
            targets: '#wear-me .content-inner .description',
            opacity: [0,1],
            translateY: [300, 0],
            easing: "linear",
            duration: 500,
            endDelay: 300,
            complete: function() {
                $('#wear-me .content-inner .dashes .dividers').removeClass('hidden');
            }
        })
        .add({
            targets: '#wear-me .content-inner .dashes .dividers',
            opacity: [0,1],
            easing: "easeOutExpo",
            duration: 700,
            endDelay: 100,
            complete: function() {
                $('#wear-me .content-inner .dashes .dash-btn').removeClass('hidden');
            }
        })
        .add({
            targets: '#wear-me .content-inner .dashes .dash-btn',
            translateY: [20, 0],
            opacity: [0,1],
            easing: "linear",
            duration: 200,
            endDelay: 200,
            complete: function() {
                $('#wear-me .content-inner .dashes .my-show-btn').removeClass('hidden');
            }
        })
        .add({
            targets: '#wear-me .content-inner .dashes .my-show-btn',
            scale: [0, 1],
            easing: "linear",
            duration: 600,
            endDelay: 2000,
            complete: function () {
                anime({
                    targets: '#wear-me .content-inner .dashes .my-show-btn',
                    scale: [1, 1.1],
                    easing: "linear",
                    direction: 'alternate',
                    loop: true,
                    duration: 300,
                    endDelay: 0,
                    delay: 1000,
                })
            }
        });
    }
    if (currentScrollTop <= wmTop -50) {
        wearMeSection = false;
        $('#wear-me .content-inner .dash-btn').addClass('hidden');
        $('#wear-me .content-inner .my-show-btn').addClass('hidden');
        $('#wear-me .content-inner .dashes .dividers').addClass('hidden');
        $('#wear-me .content-inner .description').addClass('hidden');
        $('#wear-me .content-inner h1').addClass('hidden');
        anime.remove('#wear-me .content-inner .dashes .my-show-btn');
    }

    for (var i = 0; i < sectionIds.length; i++) {
        var wiaTop = $('#' + sectionIds[i]).offset().top;
        if (currentScrollTop + 50 >= wiaTop && !animationStarted['#' + sectionIds[i]]) {

                animationStarted['#' + sectionIds[i]] = true;
                $('#' + sectionIds[i] + ' .fm-featured-img').css('display', 'block');
                verticalFeaturedImageAnimation('#' + sectionIds[i] + ' .fm-featured-img');
                originWiaTop['#' + sectionIds[i]] = wiaTop;
            if(wW >=1024) {}

        }

        if ((currentScrollTop + 50) < originWiaTop['#' + sectionIds[i]]) {
            animationStarted['#' + sectionIds[i]] = false;
        if (directionDown) {
            $('#' + sectionIds[i] + ' .fm-featured-img').css('display', 'none');
        }
        }
    }

});



function verticalFeaturedImageAnimation(target) {
    var y = 0;
    // if (target.indexOf('what-i-am-about') > -1) {
    //     y = -400;
    // }
    anime({
        targets: target,
        translateY: {
            value:    [300, y],
            easing:   'linear',
        },
        duration: 1000,
        easing: 'linear',
        opacity: {
            value: [0, 1],
            easing:   'linear',
        },
        autoplay: true,
    });
}

function startBallAnimation(x, y, rangeY, stepX, rotate, bouncingRate, rotateAngle) {
    setTimeout(function() { $('.wid-basketball-section img').removeClass('hidden') }, 30);
    var bounceUp = anime({
        autoplay: false,    //We don't want to immediately start the animation
        targets:  '.img-basketball',  //target the div '#ball'
        translateY: {
            value:    [rangeY,  (rangeY - y * bouncingRate)], //When bouncing up, start at 200px and end at 0px
            easing:   'easeOutSine',
        },
        translateX: {
            value:    [x, x - (stepX/2)],
            easing:   'linear',
        },
        rotate: {
            value: [rotate, rotate + rotateAngle/2],
            duration:350,
            easing: 'linear'
        },
        duration: 350,
        complete:  function(){
            bounceDown.play();
        }, //After we bounce up, start the bounce down animation
    });

    var bounceDown = anime({
        autoplay: false,    //See similar comments above
        targets:  '.img-basketball',
        translateY: {
            value:    [ (rangeY - y * bouncingRate ), rangeY ], //When bouncing down, start at 0px and end at 160px
            easing:   'easeInSine',
        },
        translateX: {
            value:    [(x - stepX/2), (x - stepX)],
            easing:   'linear',
        },
        rotate: {
            value: [rotate + rotateAngle/2, rotate + rotateAngle],
            duration: 350,
            easing: 'linear'
        },
        duration: 350,
        complete:  function() {
        }, //After we bounce down, start the bounce up animation
    });

    bounceUp.play(); //Start the animation!
}
